tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer number = 0;
}
prog    : (e+=zakres | e+=expr | d+=decl)* -> program(name={$e},deklaracje={$d})// "<deklaracje;separator=\"\n\"> start: <name;separator=\" \n\"> ";
;

zakres  : ^(BEGIN {locals.enterScope();} (e+=zakres | e+=expr | d+=decl)* {locals.leaveScope();}) -> blok(wyr={$e},dekl={$d})
;

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> odejmin(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> mnoz(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> dziel(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) -> podst(n={$i1.text},i={$e2.st})
        | ^(WIE   e1=expr e2=expr) -> wie(p1={$e1.st},p2={$e2.st})
        | ^(MNI   e1=expr e2=expr) -> mni(p1={$e1.st},p2={$e2.st})
        | ^(WIEROW  e1=expr e2=expr) -> wierow(p1={$e1.st},p2={$e2.st})
        | ^(MNIROW  e1=expr e2=expr) -> mnirow(p1={$e1.st},p2={$e2.st})
        | ^(EQU  e1=expr e2=expr) -> rowne(p1={$e1.st},p2={$e2.st})
        | ^(DIF  e1=expr e2=expr) -> rozne(p1={$e1.st},p2={$e2.st})
        | INT  {number++;}         -> int(i={$INT.text},j={number.toString()})
        | ID                       -> id(n={$ID.text})
        | ^(IF e1=expr e2=expr e3=expr?) {number++;} -> if(e1={$e1.st}, e2={$e2.st}, e3={$e3.st}, nr={number.toString()})
    ;





    