grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat | blok)+ EOF!;

blok  : BEGIN^ (stat | blok)* END!
;

stat
    : expr NL -> expr

    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | PRINT expr NL -> ^(PRINT expr)
    | if_stat NL -> if_stat

    | NL ->
    ;

if_stat
    : IF^ LP! expr RP! (expr) (ELSE! (expr))?
    ;

expr
    : addExpr
      ( WIE^ addExpr
      | MNI^ addExpr
      | WIEROW^ addExpr
      | MNIROW^ addExpr
      | EQU^ addExpr
      | DIF^ addExpr
      )*
    ;

addExpr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : powExpr
      ( MUL^ powExpr
      | DIV^ powExpr
      )*
    ;

powExpr
    : atom (POW^ atom)*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

VAR :'var';

IF :'if';

THEN :'then';

ELSE :'else';

BEGIN : '{';

END : '}';

PRINT : 'print';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;

LP
  : '('
  ;

RP
  : ')'
  ;

PODST
  : '='
  ;

PLUS
  : '+'
  ;

MINUS
  : '-'
  ;

MUL
  : '*'
  ;

DIV
  : '/'
  ;
  
POW
  : '**'
  ;

XOR
  : '^'
  ;

WIEROW
  : '>='
  ;

MNIROW
  : '<='
  ;

WIE
  : '>'
  ;

MNI
  : '<'
  ;

EQU
  : '=='
  ;

DIF
  : '!='
  ;

  