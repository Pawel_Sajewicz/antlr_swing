tree grammar TExpr1;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog
    : (e=expr {drukuj ($e.text);}
    |  d=decl {drukuj ($d.text);}
    |  print {})*
    ;

decl
    :  ^(VAR i1=ID) {globals.newSymbol($i1.text);}
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr returns [Integer out]
        : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;}
        | ^(AND   e1=expr e2=expr) {$out = $e1.out & $e2.out;}
        | ^(OR    e1=expr e2=expr) {$out = $e1.out | $e2.out;}
        | ^(XOR   e1=expr e2=expr) {$out = $e1.out ^ $e2.out;}
        | ^(POW   e1=expr e2=expr) {$out = (int)Math.pow($e1.out, $e2.out);}
        | ^(PODST i1=ID   e2=expr) {$out = $e2.out;
                                    globals.setSymbol($ID.text, $e2.out);}
        | INT                      {$out = getInt($INT.text);}
        | ID                       {if (globals.hasSymbol($ID.text))
                                      $out = globals.getSymbol($ID.text);}
        ;

print
        : ^(PRINT e1=expr) {drukuj ($e1.text + " = " + $e1.out.toString());}
        ;